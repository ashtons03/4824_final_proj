Directions: run the starter code in genre_classification.ipynb
    In this file, we tested 3 different things:
        different knn k-values
        test / train split accuracy
        bagging
    To run each one, highlight the code section and use (ctrl + /) to un note a code section.
    warning: running this file takes very long

We also tested other sklearn models on the dataset in the sk_models.ipynb file
k_val_graph has the results of using different k-values using the starter code
split_data_graph has the results of our test / train split accuracy tests
